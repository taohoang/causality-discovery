          used (Mb) gc trigger (Mb) max used (Mb)
Ncells   89211  2.4     407500 10.9   358035  9.6
Vcells 2017011 15.4    2829439 21.6  2265958 17.3
[1] "Running parallel-PC..."
n=88,p=1380
Num CI Tests= 1354126 26176448 576229 117 10 ,Total CI Tests= 28106930 ,Total Time= 499.161 
Object of class 'pcAlgo', from Call: 
 pc_parallel(suffStat = suffStat, indepTest = gaussCItest, alpha = 0.01,      p = p, skel.method = "parallel", num.cores = 4) 
Number of undirected edges:  181 
Number of directed edges:    450 
Total number of edges:       631 
