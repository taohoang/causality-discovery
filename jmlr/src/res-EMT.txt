          used (Mb) gc trigger (Mb) max used (Mb)
Ncells   89024  2.4     350000  9.4   311218  8.4
Vcells 1528334 11.7    2146115 16.4  1645688 12.6
[1] "Running parallel-PC..."
n=47,p=1190
Num CI Tests= 987654 3998431 9024 16 ,Total CI Tests= 4995125 ,Total Time= 105.34 
Object of class 'pcAlgo', from Call: 
 pc_parallel(suffStat = suffStat, indepTest = gaussCItest, alpha = 0.01,      p = p, skel.method = "parallel", num.cores = 4) 
Number of undirected edges:  148 
Number of directed edges:    382 
Total number of edges:       530 
