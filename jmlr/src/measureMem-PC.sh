#!/bin/bash

./maxRSS.sh Rscript runPC.R
if [[ "$OSTYPE" == "linux-gnu" ]]; then
  sudo sysctl -w vm.drop_caches=3
  sudo sync && echo 3 | sudo tee /proc/sys/vm/drop_caches
elif [[ "$OSTYPE" == "darwin"* ]]; then
  sudo purge
fi
